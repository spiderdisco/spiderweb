import fs from 'fs';
import fastifyStatic from 'fastify-static';

import { SpiderwebModule } from '../../module/module';


export const StaticModule: SpiderwebModule = {
	launch: launcher => {
		const log = launcher.createLogger('static');
		const staticDir = launcher.resolvePath(launcher.settings.staticDir);

		if(!fs.existsSync(staticDir)) {
			fs.mkdirSync(staticDir);
			log.info(`created static dir ${staticDir}`);
		}

		launcher.server.register(fastifyStatic, {
			prefix: '/',
			root: staticDir,
		});

		log.info(`serving ${staticDir} on /`);
	}
};
