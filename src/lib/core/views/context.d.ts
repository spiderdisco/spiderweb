export type ViewContextValue =  boolean | number | string | object;
export type ViewContextValueFunction = () => ViewContextValue | Promise<ViewContextValue>;

export type ViewContext = {
	[key: string]: ViewContextValue | ViewContextValueFunction,
};

export type ViewContextFunction = () => ViewContext | Promise<ViewContext>;
