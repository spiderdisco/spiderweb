import { ViewContextFunction, ViewContext } from './context';

export type ViewSettings = {
	url: string;
	template: string;
	context?: ViewContext | ViewContextFunction,
}
