import fs from 'fs';
import path from 'path';
import nunjucks from 'nunjucks';

import { SpiderwebModule } from '../../module/module';


export const ViewsModule: SpiderwebModule = {
	launch: launcher => {
		const log = launcher.createLogger('views');

		const templateDir = launcher.resolvePath(launcher.settings.templateDir);

		const njk = nunjucks.configure(templateDir, {
			noCache: true,
			watch: launcher.settings.debug
		});


		const urls = new Set<string>();
		launcher.settings.views.forEach(view => {
			if(urls.has(view.url)) {
				launcher.fail(`duplicate view url: ${view.url}`, log);
				return;
			}
			urls.add(view.url);

			if(!fs.existsSync(path.join(templateDir, view.template))) {
				launcher.fail(`missing template: ${view.template}`);
				return;
			}

			launcher.server.get(view.url, async (req, reply) => {
				reply.type('text/html');

				let context = {
					...req.params,
					...(await processContext(view.context)),
				};

				const body = njk.render(view.template, context);

				reply.send(body);
			});

			log.info(`viewing ${view.template} on ${view.url}`);
		});
	},
};

async function processContext(ctx: object | Function) {
	if(ctx instanceof Function) {
		return await ctx();
	} else {
		const result = {};
		for(const k in ctx) {
			const v = ctx[k];
			if(v instanceof Function) {
				result[k] = await v();
			} else {
				result[k] = v;
			}
		}
		return result;
	}
}
