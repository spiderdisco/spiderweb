import fastify = require('fastify');
import chalk from 'chalk';

import { SpiderwebLauncher } from '../launcher';


export interface SpiderwebLogger {
	info(message: string): void;
	warn(message: string): void;
	error(message: string): void;
	error(message: string, err: Error): void;
}

export class DevLogger implements SpiderwebLogger {
	private readonly loggerCategory: string;
	private readonly consoleCategory: string;
	private readonly logger: fastify.Logger;

	constructor(category: string, launcher: SpiderwebLauncher) {
		this.loggerCategory = `${category}: `;
		this.consoleCategory = `[ ${category} ] `;
		this.logger = launcher.server.log;
	}
	private log(level: string, message: string, fmt: (s: string) => string) {
		this.logger[level](this.loggerCategory, message);
		console.log(fmt(this.consoleCategory + message));
	}

	info(message: string): void {
		this.log('info', message, chalk.dim);
	}

	warn(message: string): void {
		this.log('warn', message, chalk.yellow);
	}

	error(message: string): void;
	error(message: string, err: Error): void;
	error(message: any, err?: Error) {
		this.log('error', message, chalk.bold.red);

		if(err) {
			this.logger.error(err);
			console.log(chalk.bold.red(err.message));
			console.error(err.stack);
		}
	}

}
