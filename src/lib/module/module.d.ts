import { SpiderwebLauncher } from '../launcher';
import { SpiderwebSettings } from '../config';


export interface SpiderwebModule {
	init?(settings: SpiderwebSettings): void | Promise<void>;
	launch(launcher: SpiderwebLauncher): void | Promise<void>;
}
