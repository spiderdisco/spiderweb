import fastify = require('fastify');

import { SpiderwebSettings } from '../config';
import { SpiderwebLogger } from '../util/logger';
import { SpiderwebRuntime } from '../runtime';


export interface SpiderwebLauncher {
	readonly server: fastify.FastifyInstance;
	readonly settings: SpiderwebSettings;
	readonly runtime: SpiderwebRuntime;

	fail(message: string, logger?: SpiderwebLogger): void;

	resolvePath(...pathSegments: string[]): string;
	createLogger(name: string): SpiderwebLogger;
}
