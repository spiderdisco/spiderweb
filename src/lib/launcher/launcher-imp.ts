import path from 'path';
import fs from 'fs';
import fastify from 'fastify';

import { SpiderwebSettings } from '../config';
import { DevLogger, SpiderwebLogger } from '../util/logger';
import { SpiderwebLauncher } from '../launcher';
import { SpiderwebModule } from '../module/module';
import { StaticModule } from '../core/static';
import { ViewsModule } from '../core/views';
import { SpiderwebRuntime } from '../runtime';


export class LauncherImp implements SpiderwebLauncher {
	readonly server: fastify.FastifyInstance;
	readonly runtime: SpiderwebRuntime;
	readonly failures: string[] = [];

	readonly coreModules: SpiderwebModule[] = [];

	constructor(
		public readonly rootDir: string,
		public readonly settings: SpiderwebSettings,
	) {
		if(!fs.existsSync(rootDir)) {
			throw new Error(`no such directory as ${rootDir}`);
		}
		if(!fs.statSync(rootDir).isDirectory()) {
			throw new Error(`${rootDir} is not a directory`);
		}

		this.server = fastify({
			logger: {
				file: this.resolvePath(this.settings.logFile),
			}
		});

		this.runtime = {};


		this.coreModules = [
			StaticModule,
			ViewsModule,
		];
	}

	fail = (message: string, log?: SpiderwebLogger) => {
		this.failures.push(message);
		if(log) {
			log.error(message);
		}
	}

	private hasFailed() { return this.failures.length !== 0; }

	launch = async (): Promise<string> => {
		// Init modules
		this.coreModules.forEach(this.initModule);

		// Launch modules
		this.coreModules.forEach(m => m.launch(this));

		// Handle module launch failure
		if(this.hasFailed()) {
			throw new Error('Failed to launch');
		}

		// Listen
		if(this.settings.socket !== undefined) {
			return await this.server.listen(this.settings.socket);
		} else {
			return await this.server.listen({
				port: this.settings.port,
				host: this.settings.host,
			});
		}
	}

	close = () => {
		this.server.close();
	}


	///////////////////////////////////////////////////////////////////////////////
	// Modules
	///////////////////////////////////////////////////////////////////////////////
	async initModule(module: SpiderwebModule) {
		if(module.init) {
			await module.init(this.settings);
		}
	}


	///////////////////////////////////////////////////////////////////////////////
	// Utils
	///////////////////////////////////////////////////////////////////////////////
	resolvePath = (...pathSegments: string[]) => path.resolve(this.rootDir, ...pathSegments);
	createLogger = (name: string): SpiderwebLogger => new DevLogger(name, this);
}
