import { SpiderwebSettings } from './config';
import { LauncherImp } from './launcher/launcher-imp';
import { StaticModule } from './core/static';
import { ViewsModule } from './core/views';


export async function spiderwebMain(rootDir: string, settings: SpiderwebSettings) {
	const launcher = new LauncherImp(rootDir, settings);
	const log = launcher.createLogger('sw');

	launcher.server.log.info(settings);

	const close = () => {
		launcher.close();
		console.log('closed.');
	};
	process.on('SIGINT', close);
	process.on('SIGTERM', close);

	const addr = await launcher.launch();
	console.log(addr);
}
