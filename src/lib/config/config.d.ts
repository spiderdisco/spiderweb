import { SpiderwebSettings } from '../config';
export type SpiderwebConfig = Partial<SpiderwebSettings>;
