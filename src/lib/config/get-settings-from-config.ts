import { SpiderwebConfig } from './config';
import { SpiderwebSettings } from './settings';
import { DEFAULT_SETTINGS } from './default-settings';

export function getSettingsFromConfig(config: SpiderwebConfig): SpiderwebSettings {
	return Object.assign({}, DEFAULT_SETTINGS, config);
}
