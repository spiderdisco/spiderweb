import path from 'path';

import { SpiderwebSettings } from './settings';


export const DEFAULT_SETTINGS: SpiderwebSettings = {
	siteName: 'spiderweb',
	port: 5555,
	socket: undefined,
	host: 'localhost',
	logFile: './spiderweb.log',
	debug: process.env.NODE_ENV === 'development',
	staticDir: './static/',
	staticURL: '/',
	templateDir: './templates/',
	views: [],
};
