import { ViewSettings } from '../core/views/settings';

export type SpiderwebSettings = {
	/**
	 * The name of the site. Used in the cli & the admin.
	 *
	 * **default:** `spiderweb`.
	 */
	siteName: string;

	/**
	 * Port on which the site will be served.
	 *
	 * This settings is ignored if `socket` is defined.
	 *
	 * **default**: `5555`
	 * */
	port: number;

	/**
	 * Address on which the site will be served.
	 *
	 * This setting is ignored if `socket` is defined.
	 *
	 * **default**: `localhost`
	 */
	host: string;

	/**
	 * Path to unix socket.
	 *
	 * If this setting is defined, it overrides `port` & `host`.
	 *
	 * **default**: `undefined`
	 */
	socket?: string;

	/**
	 * Path to the file to which the log will be written.
	 *
	 * **default**: `./spiderweb.log`
	 */
	logFile: string;


	/**
	 * Weather to run in debug mode.
	 *
	 * **default**: `process.env.NODE_ENV === 'development'`
	 */
	debug: boolean;


	/**
	 * Path to directory containing static files.
	 *
	 * **default**: `./static/`
	 */
	staticDir: string;

	/**
	 * URL on which static files will be server.
	 *
	 * **default**: `/`
	 */
	staticURL: string;


	/**
	 * Directory containing view templates.
	 *
	 * **default**: `./templates/`
	 */
	templateDir: string;

	/**
	 * The site's views.
	 *
	 * **default**: `[]`
	 */
	views: ViewSettings[];
};
