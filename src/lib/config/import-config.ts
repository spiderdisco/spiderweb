import path from 'path';
import fs from 'fs';


export type ImportConfigResult = object | 'missing-default' | 'non-object' | Error;

export default async function importConfig(configFilePath: string): Promise<ImportConfigResult> {
	if(!path.isAbsolute(configFilePath)) {
		throw new Error('importConfig was passed a relative path');
	}

	if(!fs.existsSync(configFilePath)) {
		throw new Error('importConfig was passed a non-existant file');
	}

	let imported: unknown;
	try {
		imported = await import(configFilePath);
	} catch(err) {
		return err;
	}

	if(typeof imported['default'] === 'undefined') {
		return 'missing-default';
	}

	const config: unknown = imported['default'];

	if(typeof config !== 'object') {
		return 'non-object';
	}

	return config;
}
