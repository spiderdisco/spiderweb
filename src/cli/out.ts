import chalk from 'chalk';

export namespace out {
	export function error(msg: string) {
		console.log(chalk.bold.red(msg));
	}

	export function success(msg: string) {
		console.log(chalk.bold.green(msg));
	}
}
