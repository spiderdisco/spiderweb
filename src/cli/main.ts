#!/usr/bin/env -S node -r "ts-node/register"

import path from 'path';

import importConfig from '../lib/config/import-config';
import { out } from './out';
import { getSettingsFromConfig } from '../lib/config/get-settings-from-config';
import { LauncherImp } from '../lib/launcher/launcher-imp';
import { StaticModule } from '../lib/core/static';
import { ViewsModule } from '../lib/core/views';
import { spiderwebMain } from '../lib/main';


(async () => {
	const dir = process.cwd();
	const configFile = path.resolve(dir, 'spiderweb.config.ts');

	const config = await importConfig(configFile);

	if(!(config instanceof Object) || config instanceof Error) {
		out.error('Error in config file:');

		if(config === 'missing-default') {
			out.error('  Expected config as default export.');
		} else if(config === 'non-object') {
			out.error('  Expected object as default export.');
		} else if(config instanceof Error) {
			out.error(`  Error thrown: ${config.message}`);
		console.error(config.stack);
		}
		process.exit(1);
		return;
	}

	const settings = getSettingsFromConfig(config);

	await spiderwebMain(dir, settings);

})();
