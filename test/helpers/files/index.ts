import path from 'path';


export namespace TestFiles {
	const FILE_DIR = path.join(__dirname, 'files');
	const TMP_DIR = path.join(__dirname, 'tmp');

	export const NONEXISTANT_FILE = path.join(FILE_DIR, 'nothing');
	export const EXISTANT_FILE = path.join(FILE_DIR, 'existant-file');
	export const EXISTANT_DIR = path.join(FILE_DIR, 'existant-dir');

	export const VALID_CONFIG_FILE = path.join(FILE_DIR, 'config', 'valid.config.ts');
	export const VALID_CONFIG_FILE_KEY = 'key';
	export const VALID_CONFIG_FILE_VALUE = 'value';
	export const MISSING_DEFAULT_CONFIG_FILE = path.join(FILE_DIR, 'config', 'missing-default.config.ts');
	export const THROWING_CONFIG_FILE = path.join(FILE_DIR, 'config', 'throwing.config.ts');
	export const THROWING_CONFIG_ERROR_MESSAGE = 'thrown from config';
	export const NON_OBJECT_DEFAULT_CONFIG_FILE = path.join(FILE_DIR, 'config', 'non-object-default.config.ts');

	export const STATIC_TEST_DIR = path.join(FILE_DIR, 'static');
	export const STATIC_DIR_TEST_FILE = 'test.txt';
	export const STATIC_DIR_TEST_FILE_CONTENT = 'test content\n';

	export const VIEW_RENDER_DIR = path.join(FILE_DIR, 'view-render');
	export const VIEW_RENDER_TEMPLATE_CONTENT = '<h1>test</h1>\n';
}
