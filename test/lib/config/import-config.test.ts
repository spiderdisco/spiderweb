import 'mocha';
import chai, { expect, assert } from 'chai';
import chaiAsPromised from 'chai-as-promised';
chai.use(chaiAsPromised);

import importConfig, { ImportConfigResult } from '../../../src/lib/config/import-config';
import { TestFiles } from '../../helpers/files';


describe('importConfig()', () => {
	it('returns default export', async () => {
		let result: ImportConfigResult;
		try {
			result = await importConfig(TestFiles.VALID_CONFIG_FILE);
		} catch(err) {
			assert.fail(`error was thrown: ${err.message}`);
			return;
		}
		expect(result).to.be.instanceof(Object);
		expect(result[TestFiles.VALID_CONFIG_FILE_KEY]).to.eq(TestFiles.VALID_CONFIG_FILE_VALUE);
	});

	it('throws for relative path', () => {
		return expect(importConfig('./relative/path.ts'))
			.to.eventually.be.rejectedWith('importConfig was passed a relative path');
	});

	it('throws for non-existant file', () => {
		return expect(importConfig(TestFiles.NONEXISTANT_FILE))
			.to.eventually.be.rejectedWith('importConfig was passed a non-existant file');
	});

	it('returns "missing-default" for files with no default export', () => {
		return expect(importConfig(TestFiles.MISSING_DEFAULT_CONFIG_FILE))
			.to.eventually.eq('missing-default');
	});

	it('returns error thrown from config file', async () => {
		const result = await importConfig(TestFiles.THROWING_CONFIG_FILE);
		expect(result).to.be.instanceof(Error);
		expect((result as Error).message).to.eq(TestFiles.THROWING_CONFIG_ERROR_MESSAGE);
	});

	it('returns "non-object" for non-object default exports', () => {
		return expect(importConfig(TestFiles.NON_OBJECT_DEFAULT_CONFIG_FILE))
			.to.eventually.eq('non-object');
	});
});
