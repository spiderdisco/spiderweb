import 'mocha';
import { expect } from 'chai';
import { getSettingsFromConfig } from '../../../src/lib/config/get-settings-from-config';
import { DEFAULT_SETTINGS } from '../../../src/lib/config/default-settings';
import { SpiderwebConfig } from '../../../src/lib/config';

describe('getSettingsFromConfig()', () => {
	it('returns default settings for empty config', () => {
		expect(getSettingsFromConfig({}))
			.to.deep.eq(DEFAULT_SETTINGS);
	});

	it('returns values from passed config & default settings for others', () => {
		const cnf: SpiderwebConfig = {
			siteName: 'test site',
			port: 0,
		};

		const result = getSettingsFromConfig(cnf);

		expect(result.siteName).to.eq(cnf.siteName);
		expect(result.port).to.eq(cnf.port);
		expect(result.host).to.eq(DEFAULT_SETTINGS.host);
		expect(result.logFile).to.eq(DEFAULT_SETTINGS.logFile);
	});
});
