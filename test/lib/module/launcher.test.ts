import 'mocha';
import chai, { expect } from 'chai';
import chaiAsPromised from 'chai-as-promised';
chai.use(chaiAsPromised);

import os from 'os';

import { DEFAULT_SETTINGS } from '../../../src/lib/config/default-settings';
import { SpiderwebLauncher } from '../../../src/lib/launcher';
import { LauncherImp } from '../../../src/lib/launcher/launcher-imp';
import { SpiderwebModule } from '../../../src/lib/module/module';


describe('LauncherImp#initModule()', () => {
	let launcher: LauncherImp;

	beforeEach(() => {
		launcher = new LauncherImp(os.tmpdir(), DEFAULT_SETTINGS);
	});

	afterEach(() => {
		launcher.close();
		launcher = undefined;
	});

	it('does nothing for unimplemented', async () => {
		expect(launcher.initModule({launch: _ => {}}))
			.to.be.fulfilled;
	});

	it('calls init', async () => {
		let called = false;

		const mockmodule: SpiderwebModule = {
			init: settings => { called = true; },
			launch: _ => {},
		};

		await launcher.initModule(mockmodule);
		expect(called, 'init was called')
			.to.be.true;
	});

	it('calls init async', async () => {
		let wasCalled = false;

		const mockmodule: SpiderwebModule = {
			init: settings => {
				return new Promise((resolve, reject) => {
					setTimeout(() => {
						wasCalled=true;
						resolve();
					}, 10);
				});
			},
			launch: _ => {}
		};

		await launcher.initModule(mockmodule);
		expect(wasCalled, 'init was called async')
			.to.be.true;
	});

	it('allows updated settings', async () => {
		const mockmodule: SpiderwebModule = {
			init: settings => {
				settings.siteName = 'test';
			},
			launch: _ => {}
		};

		await launcher.initModule(mockmodule);
		expect(launcher.settings.siteName, 'updated setting')
			.to.eq('test');

	});
});
