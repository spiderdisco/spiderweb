import 'mocha';
import chai, { expect } from 'chai';
import chaiAsPromised from 'chai-as-promised';
chai.use(chaiAsPromised);

import path from 'path';
import fs from 'fs';
import os from 'os';

import { LauncherImp } from '../../../src/lib/launcher/launcher-imp';
import { TestFiles } from '../../helpers/files';
import { DEFAULT_SETTINGS } from '../../../src/lib/config/default-settings';
import { SpiderwebConfig } from '../../../src/lib';


describe('LauncherImp#launch()', () => {

	const launchers: LauncherImp[] = [];

	function testLauncher(s: SpiderwebConfig = {}) {
		const settings = {
			...DEFAULT_SETTINGS,
			...s,
			port: 0,
		};

		after(() => {
			launchers.forEach(l => l.close());
		});

		const launcher = new LauncherImp(os.tmpdir(), settings);
		launchers.push(launcher);

		return launcher;
	}

	it('throws if hasFailed', () => {
		const launcher = testLauncher();
		launcher.fail('');

		return expect(launcher.launch())
			.to.eventually.be.rejectedWith('Failed to launch');
	});

	it('returns address string', () => {
		const launcher = testLauncher();

		return expect(launcher.launch())
			.to.eventually.match(/^http:\/\/127\.0\.0\.1:\d\d\d\d\d?\d?$/);
	});

	it('responds', async () => {
		const launcher = testLauncher();
		const response = await launcher.server.inject({
			method: 'GET',
			url: '/'
		});
		expect(response.statusCode)
			.to.eq(404);
	});

	it('uses socket file, if defined in settings', async () => {
		const dir = os.tmpdir();
		const sock = path.join(dir, 'sock.sock');

		const launcher = testLauncher({socket: sock});

		const address = await launcher.launch();
		expect(address).to.eq(sock);
		expect(fs.existsSync(sock), 'socket file exists').to.be.true;
	});
});
