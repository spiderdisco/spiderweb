import 'mocha';
import { expect } from 'chai';

import fs from 'fs';
import path from 'path';
import os from 'os';

import { TestFiles } from '../../helpers/files';
import { LauncherImp } from '../../../src/lib/launcher/launcher-imp';
import { DEFAULT_SETTINGS } from '../../../src/lib/config/default-settings';

describe('LauncherImp#ctor()', () => {
	it('throws for non-existant rootDir', () => {
		expect(() => new LauncherImp(TestFiles.NONEXISTANT_FILE, DEFAULT_SETTINGS))
			.to.throw('no such directory as');
	});

	it('throws for non-directory rootDir', () => {
		expect(() => new LauncherImp(TestFiles.EXISTANT_FILE, DEFAULT_SETTINGS))
			.to.throw('is not a directory');
	});

	it('instantiates fastify', () => {
		expect(new LauncherImp(TestFiles.EXISTANT_DIR, DEFAULT_SETTINGS).server)
			.to.be.not.undefined;
	});

	it('instantiates logger', () => {
		const dir = os.tmpdir();

		const l = new LauncherImp(dir, DEFAULT_SETTINGS);
		l.server.log.info('test');

		expect(fs.existsSync(path.join(dir, DEFAULT_SETTINGS.logFile)))
			.to.be.true;
	});
});
