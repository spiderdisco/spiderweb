import 'mocha';
import { expect } from 'chai';

import path from 'path';

import { LauncherImp } from '../../../src/lib/launcher/launcher-imp';
import { TestFiles } from '../../helpers/files';
import { DEFAULT_SETTINGS } from '../../../src/lib/config/default-settings';

describe('LauncherImp#resolvePath()', () => {
	it('returns path relative to rootDir for relative path', () => {
		const result = new LauncherImp(TestFiles.EXISTANT_DIR, DEFAULT_SETTINGS)
			.resolvePath('some', 'relative', 'path');

		expect(result)
			.to.eq(path.join(TestFiles.EXISTANT_DIR, 'some', 'relative', 'path'));
	});

	it('returns absolute path for absolute path', () => {
		const result = new LauncherImp(TestFiles.EXISTANT_DIR, DEFAULT_SETTINGS)
			.resolvePath('/', 'abs', 'path');

		expect(path.isAbsolute(result), 'result is not absolute')
			.to.be.true;

		expect(result)
			.to.eq(path.join('/', 'abs', 'path'));
	});
});
