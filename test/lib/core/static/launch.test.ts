import 'mocha';
import chai, { expect } from 'chai';

import path from 'path';
import fs from 'fs';
import os from 'os';

import { SpiderwebLauncher } from '../../../../src/lib/launcher';
import { LauncherImp } from '../../../../src/lib/launcher/launcher-imp';
import { DEFAULT_SETTINGS } from '../../../../src/lib/config/default-settings';
import { TestFiles } from '../../../helpers/files';
import { StaticModule } from '../../../../src/lib/core/static';


describe('StaticModule.launch()', () => {
	let launcher: LauncherImp;

	afterEach(() => {
		if(launcher) {
			launcher.close();
			launcher = undefined;
		}
	});

	it('creates static directory if it does not exist', () => {
		const dir = os.tmpdir();

		launcher = new LauncherImp(dir, DEFAULT_SETTINGS);
		StaticModule.launch(launcher);

		expect(fs.existsSync(path.join(dir, 'static')), 'static dir exists')
			.to.be.true;
	});

	it('serves static files', async () => {
		launcher = new LauncherImp(TestFiles.STATIC_TEST_DIR, DEFAULT_SETTINGS);
		StaticModule.launch(launcher);

		const response = await launcher.server.inject({
			method: 'GET',
			url: `/${TestFiles.STATIC_DIR_TEST_FILE}`,
		});

		expect(response.statusCode).to.eq(200);
		expect(response.payload).to.eq(TestFiles.STATIC_DIR_TEST_FILE_CONTENT);
	});
});
