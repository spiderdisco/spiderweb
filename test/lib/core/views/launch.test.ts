import 'mocha';
import chai, { expect } from 'chai';

import os from 'os';

import { LauncherImp } from '../../../../src/lib/launcher/launcher-imp';
import { DEFAULT_SETTINGS } from '../../../../src/lib/config/default-settings';
import { ViewsModule } from '../../../../src/lib/core/views';
import { SpiderwebConfig } from '../../../../src/lib';

describe('ViewsModule#launch()', () => {

	let launcher: LauncherImp;

	function testLauncher(config: SpiderwebConfig = {}) {
		launcher = new LauncherImp(os.tmpdir(), {...DEFAULT_SETTINGS, ...config});
	}

	afterEach(() => {
		if(launcher) {
			launcher.close();
			launcher = undefined;
		}
	});

	it('fails for duplicate urls', async () => {
		testLauncher({
			views: [
				{
					url: '/',
					template: 'index.html'
				}, {
					url: '/',
					template: 'index.html'
				}
			]
		});

		await ViewsModule.launch(launcher);

		expect(launcher.failures)
			.to.include('duplicate view url: /');
	});

	it('fails for missing template', async () => {
		testLauncher({
			views: [
				{
					url: '/',
					template: 'index.html'
				}
			]
		});

		await ViewsModule.launch(launcher);
		expect(launcher.failures)
			.to.include('missing template: index.html');
	});
});
