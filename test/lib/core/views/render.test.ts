import 'mocha';
import chai, { expect } from 'chai';

import { TestFiles } from '../../../helpers/files';
import { LauncherImp } from '../../../../src/lib/launcher/launcher-imp';
import { DEFAULT_SETTINGS } from '../../../../src/lib/config/default-settings';
import { ViewsModule } from '../../../../src/lib/core/views';
import { SpiderwebLauncher } from '../../../../src/lib/launcher';
import { ViewSettings } from '../../../../src/lib/core/views/settings';


describe('template rendering', () => {
	let launcher: SpiderwebLauncher;

	function testLauncher(views: ViewSettings[]) {
		launcher = new LauncherImp(TestFiles.VIEW_RENDER_DIR, {
			...DEFAULT_SETTINGS, views,
		});
	}

	it('renders contextless template', async () => {
		testLauncher([
			{
				url: '/',
				template: 'index.html'
			},
		]);

		ViewsModule.launch(launcher);

		const response = await launcher.server.inject({
			method: 'GET',
			url: '/'
		});

		expect(response.statusCode).to.eq(200);
		expect(response.payload).to.eq('<h1></h1>\n');
	});

	it('renders context with value', async () => {
		testLauncher([
			{
				url: '/',
				template: 'index.html',
				context: {
					title: 'test'
				}
			}
		]);

		ViewsModule.launch(launcher);

		const response = await launcher.server.inject({
			method: 'GET',
			url: '/'
		});

		expect(response.statusCode).to.eq(200);
		expect(response.payload).to.eq('<h1>test</h1>\n');
	});

	it('renders context with value function', async () => {
		testLauncher([
			{
				url: '/',
				template: 'index.html',
				context: {
					title: () => 'test'
				}
			}
		]);

		ViewsModule.launch(launcher);

		const response = await launcher.server.inject({
			method: 'GET',
			url: '/'
		});

		expect(response.statusCode).to.eq(200);
		expect(response.payload).to.eq('<h1>test</h1>\n');
	});

	it('renders context with async value function', async () => {
		testLauncher([
			{
				url: '/',
				template: 'index.html',
				context: {
					title: () => new Promise((resolve, reject) => {
						setTimeout(() => {
							resolve('test');
						}, 10);
					})
				}
			}
		]);

		ViewsModule.launch(launcher);

		const response = await launcher.server.inject({
			method: 'GET',
			url: '/'
		});

		expect(response.statusCode).to.eq(200);
		expect(response.payload).to.eq('<h1>test</h1>\n');
	});

	it('renders context with context function', async () => {
		testLauncher([
			{
				url: '/',
				template: 'index.html',
				context: () => ({
					title: 'test',
				})
			}
		]);

		ViewsModule.launch(launcher);

		const response = await launcher.server.inject({
			method: 'GET',
			url: '/'
		});

		expect(response.statusCode).to.eq(200);
		expect(response.payload).to.eq('<h1>test</h1>\n');
	});

	it('renders context with async context function', async () => {
		testLauncher([
			{
				url: '/',
				template: 'index.html',
				context: () => new Promise((resolve, reject) => {
					setTimeout(() => {
						resolve({
							title: 'test',
						});
					}, 10);
				})
			}
		]);

		ViewsModule.launch(launcher);

		const response = await launcher.server.inject({
			method: 'GET',
			url: '/'
		});

		expect(response.statusCode).to.eq(200);
		expect(response.payload).to.eq('<h1>test</h1>\n');
	});

	it('context contains url params', async () => {
		testLauncher([
			{
				url: '/:title',
				template: 'index.html',
			}
		]);

		ViewsModule.launch(launcher);

		const response = await launcher.server.inject({
			method: 'GET',
			url: '/test'
		});

		expect(response.statusCode).to.eq(200);
		expect(response.payload).to.eq('<h1>test</h1>\n');
	});
});
